<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report Submission</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style>
.error{
color:red;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="report">Report Screen</a>
			<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
		</div>
	</div>
	<br />
	<br />
	<br /><br /><br />
	<br />
	<br />
	<div align="center">
<form:form action="credit" modelAttribute="report">
<table border="0">
			<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1">Report Management </h2></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><p class="heading3">Fill the Report Details</p></td>
				</tr>
				<tr><td><form:label path="">Batch Id</form:label></td>
				<td><form:input path="" value="${id}"/></td>
				</tr>
				
				<tr>
				<td><form:label path="rating">Rating</form:label></td>
				<td><form:input path="rating"/><form:errors cssClass="error" path="rating"/></td>
				</tr>
				
	            <tr>
				<td><form:label path="noOfWorkingDays">No Of Working Days</form:label></td>
				<td><form:input path="noOfWorkingDays"/><form:errors cssClass="error" path="noOfWorkingDays"/></td>
				</tr>
				
				<tr>
				<td><form:label path="statusOfReportSubmission">Status Of Report </form:label></td>
				<td><form:radiobutton path="statusOfReportSubmission"/>Yes
				<form:radiobutton path="statusOfReportSubmission"/>No
				<form:errors cssClass="error" path="statusOfReportSubmission"/></td>
				</tr>

                 <tr>
                 <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                 </tr>
</table>
</form:form></div>
</body>
</html>
