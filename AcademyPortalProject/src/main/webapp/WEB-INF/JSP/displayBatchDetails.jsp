<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  
    <link rel="stylesheet" href=
"https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
  
     <script type="text/javascript" 
     src="https://code.jquery.com/jquery-3.5.1.js">
     </script>
  
     <script type="text/javascript" src=
"https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js">
     </script>
<title>Batch Details</title>
<script>
        $(document).ready(function() {
            $('#tableID').DataTable({ });
        });
    </script>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="batch">Batch Allocation</a>
		</div>
	</div>
	<center>
				<p class="heading1">BatchDetails</p>
			</center>
<table id="tableID" class="display" style="width:100%">
<thead>
<tr class="thcolor"><th>Batch Id</th><th>Batch Name</th><th>Trainer Name</th><th>No Of Trainers</th><th>Domain</th><th>Batch Start Date</th><th>Batch End Date</th></tr>
</thead>
<tbody>
<c:forEach var="user" items="${listOfUsers}">  
 <tr class="tdcolor">
<td>${user.batchId}</td>
<td>${user.batchName}</td>
<td>${user.trainerName}</td>
<td>${user.noOfTrainees}</td>
<td>${user.domain}</td>
<td>${user.batchStartDate}</td>
<td>${user.batchEndDate}</td>
</tr> 
   </c:forEach>
   </tbody>
</table> 
</body>
</html>

