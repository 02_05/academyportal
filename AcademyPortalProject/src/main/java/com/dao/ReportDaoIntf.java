package com.dao;

import java.util.List;

import com.model.Batch;
import com.model.Report;

public interface ReportDaoIntf {

	List<Batch> getBatchId();

	void calculateCredit(Report report, int val);

	List<Batch> getBatchDetails();

}
