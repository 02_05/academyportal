<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Faculty Update</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.topnav {
  overflow: hidden;
  background-color: #3C4551;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  font-weight: bold;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #E15050;
  color: white;
}
.table1{
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="facultyView">Faculty Details</a>
			<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
		</div>
	</div>
	<br />
	<br />
	<br />
	<div align="center">   
<form:form action="/AcademyPortal/updateSave" style="max-width: 450px; margin:0 auto;">    
 <table border="0">
<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1">Edit Faculty Details</h2></td>
				</tr>
				<!-- <tr>
					<td colspan="2" align="center"><p class="heading3">Fill the Faculty Details</p></td>
				</tr> -->
				<tr>
				<td><form:hidden path="associateId" /> </td>
				</tr>
 
<tr>
				<td><form:label path="firstName">First Name</form:label><br/></td>
				<td><form:input path="firstName"/><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="lastName">Last Name</form:label><br/></td>
				<td><form:input path="lastName" /><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="age">Age</form:label><br/></td>
				<td><form:input path="age" /><br/></td>
				</tr>
				
				<tr>
				<td><form:label path="gender">Gender</form:label></td>
				<td><form:radiobutton path="gender" value="Male"/>Male
				<form:radiobutton path="gender" value="Female"/>Female <br/></td>
				</tr>
				
				<tr>
				<td><form:label path="contactNumber">Contact Number</form:label><br/></td>
				<td><form:input path="contactNumber" /><br/></td>
				</tr>
				
				
				<tr>
				<td><form:label path="emailId">Email Id</form:label><br/></td>
				<td><form:input type="email" path="emailId"/><br/> </td>
				</tr>
				
				<tr>
                 <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                 </tr>
				 </table>
</form:form>    </div>
</body>
</html>


<%-- <form:hidden path="associateId" /> 

<form:label path="firstName">First Name</form:label><br/>
<form:input path="firstName"/><br/>
<form:label path="lastName">Last Name</form:label><br/>
<form:input path="lastName" /><br/>
<form:label path="age">Age</form:label><br/>
<form:input path="age" /><br/>
<form:label path="gender">Gender</form:label>
<form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female <br/>
<form:label path="contactNumber">Contact Number</form:label><br/>
<form:input path="contactNumber" /><br/>
<form:label path="emailId">Email Id</form:label><br/>
<form:input type="email" path="emailId"/><br/> 
<input type="submit" value="Submit" class="btn btn-primary"> 
</form:form>    --%> 