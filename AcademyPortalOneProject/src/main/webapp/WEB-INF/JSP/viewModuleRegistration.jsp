<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ViewModuleDetails</title>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.tablepro
{border-collapse:collapse;
margin-left: auto;
margin-right: auto;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
		<a href="module">Module Registration</a>
			<a href="moduleUpdate">Module Updation</a>
		</div>
	</div>
	<br />
	<br />
	<br /><br />
	<br />
<table border="1" class="tablepro">
<tr class="thcolor"><th>Course Id</th><th>Course Name</th><th>Course Duration</th></tr>
<c:forEach var="user" items="${listOfUsers}">  
   <tr class="tdcolor">
<%-- <td>${user.userid}</td> --%>
<td>${user.courseId}</td>
<td>${user.courseName}</td>
<td>${user.courseDuration}</td>
</tr> 
   </c:forEach>
</table> 
</body>
</html>

