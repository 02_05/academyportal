<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<script>
function validate(){
	var name=document.getElementById("name").value;
	var password=document.getElementById("crctpassword").value;
	
	document.getElementById("divname").innerHTML="";
	document.getElementById("divpassword").innerHTML="";
	
	if(name==""){
		document.getElementById("divname").innerHTML="Name should not be empty!...";
		return false;
	}else if(password==""){
		document.getElementById("divpassword").innerHTML="Password should not be empty!...";
		return false;
	}
}
</script>
<style>
.error{
color:red;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Faculty Login</title>
</head>
<body>
<div class="topnav">
  <a class="active" href="#">Academy Portal</a>
  <a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
  <div class="topnav-right">
				<a href="register">Register Here</a>
			</div>
</div><br/><br/>
<img  class="img1" alt="image" src="resources/images/faculty21.jpg"><br/>
<!-- <p class="heading1">Login as a Faculty</p> -->
<form:form action="loginValidate" modelAttribute="registration" onsubmit="return validate()" style="max-width: 350px; margin:0 auto;">
<div class="form-group">
<center><p class="heading1">Login as a Faculty</p></center>
<p style="color:green">${success}</p>
<form:label path="firstName">First Name</form:label><br/>
<form:input path="firstName" id="name" class="form-control"/><br/>
<span style="color:red" id="divname"></span>
</div>
<div class="form-group">
<form:label path="password">Password</form:label><br/>
<form:password path="password" id="crctpassword" class="form-control"/><br/>
<span style="color:red" id="divpassword"></span>
</div >
<h3 style="color:red">${msg}</h3>
<input type="submit" value="Submit" class="btn btn-primary"> 
<b>  New User? </b><a href="register">Register Here</a>
</form:form>
</body>
</html>