<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Module Registration</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style>
.error
{
color:red;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
			<a href="moduleUpdate">Module Updation</a>
		
		
		<div class="topnav-right">
			<!-- <a href="index.jsp">Home</a>
			<a href="ModuleUpdate">Module Updation</a> -->
			
		</div>
	</div>
	<br />
	<br />
	<br /><br /><br />
	<br />
	<br />
	<!-- <center>
				<h2 class="heading1">Module Registration</h2>
			</center> -->
			
<div align="center">
<form:form action="saveModuleRegistration" modelAttribute="moduleregistration" style="max-width: 450px; margin:0 auto;">
<table border="0">
<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1">Module Registration</h2></td>
				</tr>
				<tr>
				     
					<td colspan="2" align="center"><p class="heading3">Fill the Module Details</p></td>
				</tr>
                <%-- <tr>
                <td><form:label path="courseId" cssClass="name">courseId</form:label><br/></td>
                <td><form:input path="courseId" class="form-control"/></td>
                <td><form:errors cssClass="error" path="courseId"/></td>
                </tr> --%>


                <tr>
                <td><form:label path="courseName" cssClass="name">courseName</form:label><br/></td>
                <td><form:input path="courseName" class="form-control"/></td>
                <td><form:errors cssClass="error" path="courseName"/></td>
                </tr>



                <tr>
                <td><form:label path="courseDuration" cssClass="name">courseDuration</form:label><br/></td>
                <td><form:input path="courseDuration" class="form-control"/></td>
                <td><form:errors cssClass="error" path="courseDuration"/></td>
                </tr>
                
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                </tr>
                



				
<%-- <div class="form-group">
<form:label path="courseId">courseId</form:label><br/>
<form:input path="courseId" class="form-control"/><form:errors cssClass="error" path="courseId"/><br/>
</div>
<div class="form-group">
<form:label path="courseName">courseName</form:label><br/>
<form:input path="courseName" class="form-control"/><form:errors cssClass="error" path="courseName"/><br/>
</div>
<div class="form-group">
<form:label path="courseDuration">courseDuration</form:label><br/>
<form:input path="courseDuration" class="form-control"/><form:errors cssClass="error" path="courseDuration"/><br/>
</div>
<input type="submit" value="Submit"/>
 --%></table>

</form:form></div>
</body>
</html>

