<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Batch Details</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.tablepro
{border-collapse:collapse;
margin-left: auto;
margin-right: auto;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		
		
		<div class="topnav-right">
			<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
		</div>
	</div>
	<br />
	<br />
	<br /><br />
	<br />
	
<table border="1" class="tablepro">
<caption>Batch Details</caption>
<tr class="thcolor"><th>Batch Id</th><th>Batch Name</th><th>Trainer Name</th><th>No Of Trainers</th><th>Domain</th><th>Batch Start Date</th><th>Batch End Date</th><th>Faculty Credit Point</th></tr>
<c:forEach var="user" items="${batchId}">  
 <tr class="tdcolor">
<td>${user.batchId}</td>
<td>${user.batchName}</td>
<td>${user.trainerName}</td>
<td>${user.noOfTrainees}</td>
<td>${user.domain}</td>
<td>${user.batchStartDate}</td>
<td>${user.batchEndDate}</td>
<td>${user.report.facultyCreditPoint}</td>
 </tr> 
   </c:forEach>
</table> 
</body>
</html>




