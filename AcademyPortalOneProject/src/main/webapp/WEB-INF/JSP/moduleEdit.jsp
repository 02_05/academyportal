<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Module Registration</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style type="text/css">
.topnav {
  overflow: hidden;
  background-color: #3C4551;
}

.topnav a {
  float: left;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
  font-weight: bold;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #E15050;
  color: white;
}



</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		<a href="index.jsp"><i class="fa fa-fw fa-home"></i>Home</a>
		
		
		
		<div class="topnav-right">
			
		</div>
	</div>
 <div align="center"><br><br><br><br><br><br>

<form:form action="/AcademyPortal/moduleSave">
<table border="0">
			<tr>
				     
					<td colspan="2" align="center"><h2 class="heading1">Update Module Registration </h2></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><p class="heading3">Fill the Module Details</p></td>
				</tr>
				
				<tr>
				<td><form:hidden path="courseId" /></td>
				</tr>
				<tr>
				<td><form:label path="courseName">Course Name</form:label><br/></td>
				<td><form:input path="courseName"/><br/></td>
				</tr>
				<tr>
				<td><form:label path="courseDuration">Course Duration</form:label><br/></td>
				<td><form:input path="courseDuration" /><br/></td>
				</tr>


                   <tr>
                    <td colspan="2" align="center"><input type="submit" value="Submit" class="btn btn-primary"/></td>
                </tr>

</table>
</form:form>
</div>
</body>
</html>

