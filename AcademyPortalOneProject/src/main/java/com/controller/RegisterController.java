package com.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Batch;
import com.model.Registration;
import com.service.RegisterServiceIntf;

@Controller
public class RegisterController {
	@Autowired
	RegisterServiceIntf service;
	int faculty_id;
	@RequestMapping("/register")
	public ModelAndView registerPage(@ModelAttribute("registration")Registration registration) {
		return new ModelAndView("facultyRegistration");  //display the jsp page
		
	}
	@RequestMapping("/saveDetails")
	public ModelAndView saveRegisterPage(@Validated @ModelAttribute("registration")Registration registration,BindingResult result) {
		
		if(result.hasErrors()) {
			return new ModelAndView("facultyRegistration");
		}else {
			service.saveDetails(registration);
			ModelAndView model=new ModelAndView("facultyLogin");
			model.addObject("success","Successfully Registered Please Login!...");
			return model;		
		}
	}
	@RequestMapping("/faculty")
	public ModelAndView loginPage(@ModelAttribute("registration")Registration registration) {
		return new ModelAndView("facultyLogin");  //display the jsp page
		
	}
	@RequestMapping("/loginValidate")
	public ModelAndView loginValidate(@ModelAttribute("registration")Registration registration,BindingResult result) {
		if(result.hasErrors()) {
			return new ModelAndView("facultyLogin");
		}else {
			int result1=service.checkUser(registration);
			if(result1==0) {
				ModelAndView model=new ModelAndView("facultyLogin");
				model.addObject("msg","Invalid credentials");
				return model;
			}else {
				faculty_id=result1;
				return new ModelAndView("redirect:/facultyViews");
				
			}
		}
	}
	
	@RequestMapping("/facultyViews")
	public ModelAndView reportView(@ModelAttribute("batch") Batch batch,BindingResult result) {
		List<Batch> list=service.getbfDetails(faculty_id);
		return new ModelAndView("facultyProfile","batchId",list);
		
	}
	
	@RequestMapping("/facultyView")
	public ModelAndView login(@ModelAttribute("registration")Registration registration) {
		List<Registration> ls = service.getFacultyDetails();
		return new ModelAndView("displayFacultyDetails", "listOfUsers", ls);
	}
	
	
	@RequestMapping(value="/update/{id}")    
    public ModelAndView edit(@PathVariable int id,Registration registration,Model model){
		registration=service.getDetails(id);
		model.addAttribute("command",registration);
		return new ModelAndView("editForm");    
    }    
	
	
	@RequestMapping("/updateSave")    
    public ModelAndView editSave(Registration registration){
		service.edit(registration);
		return new ModelAndView("redirect:/facultyView");    
    }   
	
	@RequestMapping(value="/delete/{id}")    
    public ModelAndView delete(@PathVariable int id){
		service.delete(id);
		return new ModelAndView("redirect:/facultyView");    
    }    
}
