package com.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Batch;
import com.model.Registration;
import com.model.Report;
import com.service.ReportServiceIntf;

@Controller
public class ReportController {
	@Autowired
	ReportServiceIntf service;
	int val;
	@RequestMapping("/report")
	public ModelAndView reportPage(@ModelAttribute("batch") Batch batch,BindingResult result) {
		List<Batch> list=service.getBatchId();
		return new ModelAndView("reportScreen","batchId",list);
		
	}
	@RequestMapping("/reportSubmission")
	public ModelAndView reportSubmissionPage(@Validated @ModelAttribute("report") Report report,BindingResult result,Batch batch) {
		val=batch.getBatchId();
		return new ModelAndView("reportSubmissionPage","id",batch.getBatchId());
	}
	@RequestMapping("/credit")
	public ModelAndView creditPoint(@ModelAttribute("report") Report report,BindingResult result) {
		service.calculateCredit(report,val);
		return new ModelAndView("redirect:/reportViews");
		//return new ModelAndView("reportView");
	}
	@RequestMapping("/reportViews")
	public ModelAndView reportView(@ModelAttribute("batch") Batch batch,BindingResult result) {
		List<Batch> list=service.getBatchDetails();
		return new ModelAndView("reportView","batchId",list);
		
	}
}
