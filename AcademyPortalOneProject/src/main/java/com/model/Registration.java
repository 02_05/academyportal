package com.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

/*firstName,lastName,age,gender,contactNumber,emailId,password,AssociateId*/
@Entity
@SequenceGenerator(name="seq",initialValue=10001)
@Table(name="FACULTY_Tabl")
public class Registration {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY,generator="seq")
	private int associateId;
	@NotEmpty(message="FirstName should not be empty")
	private String firstName;
	@NotEmpty(message="LastName should not be empty")
	private String lastName;
	//@NotBlank(message="Age should not be empty")
	private int age;
	@NotEmpty(message="Gender should not be empty")
	private String gender;
	//@NotNull(message="ContactNumber should not be empty")
	private long contactNumber;
	//@Column(name = "emailId", unique = true, nullable = false, length = 100)
	@NotEmpty(message="EmailId should not be empty")
	private String emailId;
	@NotEmpty(message="Password should not be empty")
	private String password;
	@OneToMany(cascade=CascadeType.ALL)
	//@JoinColumn(name="Associate_Id")
	private List<Batch> batch;
	public Registration() {
		super();
	}
	public Registration(int associateId, String firstName, String lastName, int age, String gender, long contactNumber,
			String emailId, String password) {
		super();
		this.associateId = associateId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.gender = gender;
		this.contactNumber = contactNumber;
		this.emailId = emailId;
		this.password = password;
	}
	public int getAssociateId() {
		return associateId;
	}
	public void setAssociateId(int associateId) {
		this.associateId = associateId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
