package com.model;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Embeddable
public class Report {
	@NotNull(message="Rating should not be empty")
	private Integer rating;
	@NotNull(message="Rating should not be empty")
	private Integer noOfWorkingDays;
	@NotNull(message="Rating should not be empty")
	private String statusOfReportSubmission;
	private Integer facultyCreditPoint;
	public Report() {
		super();
		
	}
	
	

	public Integer getFacultyCreditPoint() {
		return facultyCreditPoint;
	}



	public void setFacultyCreditPoint(Integer facultyCreditPoint) {
		this.facultyCreditPoint = facultyCreditPoint;
	}



	
	

	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public Integer getNoOfWorkingDays() {
		return noOfWorkingDays;
	}
	public void setNoOfWorkingDays(Integer noOfWorkingDays) {
		this.noOfWorkingDays = noOfWorkingDays;
	}
	public String getStatusOfReportSubmission() {
		return statusOfReportSubmission;
	}
	public void setStatusOfReportSubmission(String statusOfReportSubmission) {
		this.statusOfReportSubmission = statusOfReportSubmission;
	}
	
	
}
