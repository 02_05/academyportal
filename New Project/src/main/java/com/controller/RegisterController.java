package com.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Registration;
import com.service.RegisterServiceIntf;

@Controller
public class RegisterController {
	@Autowired
	RegisterServiceIntf service;
	@RequestMapping("/register")
	public ModelAndView registerPage(@ModelAttribute("registration")Registration registration) {
		return new ModelAndView("facultyRegistration");  //display the jsp page
		
	}
	@RequestMapping("/saveDetails")
	public ModelAndView saveRegisterPage(@Validated @ModelAttribute("registration")Registration registration,BindingResult result) {
		
		if(result.hasErrors()) {
			return new ModelAndView("facultyRegistration");
		}else {
			service.saveDetails(registration);
			return new ModelAndView("facultyLogin");
			
		}
	}
	@RequestMapping("/faculty")
	public ModelAndView loginPage(@ModelAttribute("registration")Registration registration) {
		return new ModelAndView("facultyLogin");  //display the jsp page
		
	}
	@RequestMapping("/loginValidate")
	public ModelAndView loginValidate(@ModelAttribute("registration")Registration registration,BindingResult result) {
		if(result.hasErrors()) {
			return new ModelAndView("facultyLogin");
		}else {
			int result1=service.checkUser(registration);
			if(result1==1) {
				return new ModelAndView("success");
			}else {
				return new ModelAndView("facultyLogin");
			}
		}
	}
	
}
