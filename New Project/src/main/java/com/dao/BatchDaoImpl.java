package com.dao;
import java.util.List;




import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

 

import com.model.Batch;

 

@Repository

public class BatchDaoImpl implements BatchDaoIntf {
	//connection using hibernate
    @Autowired
    SessionFactory sessionFactory;
    //insert
    public void saveBatch(Batch batch) {
        sessionFactory.openSession().save(batch);
        
    }
    //fetch
    public List<Batch> getUsers()
    {
        List<Batch> ls=sessionFactory.openSession().createQuery("from Batch").list();
        return ls;
    }
/*@Autowired
    JdbcTemplate jdbcTemplate;
//-------using gettime------
    public void saveRegister(Register register) {
        //register.getDateOfBirth().getTime();
        java.sql.Date sqlDate=new java.sql.Date(register.getDateOfBirth().getTime());
        jdbcTemplate.update("insert into register values('"+register.getUsername()+"','"+register.getPassword()+"','"+register.getLanguages()+"','"+register.getGender()+"','"+register.getCountry()+"','"+sqlDate+"')");
        // jdbcTemplate.update("insert into register values('"+register.getUsername()+"','"+register.getPassword()+"','"+register.getLanguages()+"','"+register.getGender()+"','"+sqlDate+"')");
    }
public void saveRegister(Register register) {
    //register.getDateOfBirth().getTime();
    //java.sql.Date sqlDate=new java.sql.Date(register.getDateOfBirth().getTime());
    jdbcTemplate.update("insert into register values('"+register.getUsername()+"','"+register.getPassword()+"','"+register.getLanguages()+"','"+register.getGender()+"','"+register.getCountry()+"','"+register.getDateOfBirth()+"')");
    
}*/

}
