<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Page</title>
<style>
.error{
color:red;
}
</style>
</head>
<body>
<form:form action="saveDetails" modelAttribute="registration">
<div class="form-group">
<form:label path="firstName">First Name</form:label><br/>
<form:input path="firstName" class="form-control"/><form:errors cssClass="error" path="firstName"/><br/>
</div>
<div class="form-group">
<form:label path="lastName">Last Name</form:label><br/>
<form:input path="lastName" class="form-control"/><form:errors cssClass="error" path="lastName"/><br/>
</div>
<div class="form-group">
<form:label path="age">Age</form:label><br/>
<form:input path="age" class="form-control"/><form:errors cssClass="error" path="age"/><br/>
</div>
<div class="form-group">
<form:label path="gender">Gender</form:label>
<form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female <form:errors cssClass="error" path="gender"></form:errors><br/>
</div>
<div class="form-group">
<form:label path="contactNumber">Contact Number</form:label><br/>
<form:input path="contactNumber" class="form-control"/><form:errors cssClass="error" path="contactNumber"/><br/>
</div>
<div class="form-group">
<form:label path="emailId">Email Id</form:label><br/>
<form:input path="emailId" class="form-control"/><form:errors cssClass="error" path="emailId"/><br/>
</div>
<div class="form-group">
<form:label path="password">Password</form:label><br/>
<form:password path="password" class="form-control"/><form:errors cssClass="error" path="password"/><br/>
</div>
<input type="submit" value="Submit" class="btn btn-primary"> 
</form:form>
</body>
</html> 




 


