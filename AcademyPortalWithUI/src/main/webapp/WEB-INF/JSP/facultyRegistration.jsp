<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Faculty Registration Page</title>
<style>
.error{
color:red;
}
</style>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a> <a href="login">Admin
			Login</a> <a href="loginValidate">Faculty Login</a>
		<div class="topnav-right">
			<a href="index.jsp">Home</a>
		</div>
	</div>
	<br />
	<br />
	<center>
				<h3 class="heading1">Faculty Registration Page</h3>
			</center><br/>
<form:form action="saveDetails" modelAttribute="registration" style="max-width: 350px; margin:0 auto;">
<div class="form-group">
		
			<form:label path="firstName" cssClass="name">First Name</form:label>
			<form:input path="firstName" class="form-control" />
			<form:errors cssClass="error" path="firstName" /><br>
			<form:label path="lastName" cssClass="name">Last Name</form:label>
			<form:input path="lastName" class="form-control" />
			<form:errors cssClass="error" path="lastName" /><br>
			<form:label path="age" cssClass="name">Age</form:label>
			<form:input path="age" class="form-control" />
			<form:errors cssClass="error" path="age" /><br>
			<form:label path="gender" cssClass="name">Gender</form:label>
			 &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; 
			<form:radiobutton path="gender" value="Male" cssClass="name"/>
			<form:label path="gender" cssClass="name">Male</form:label>
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
			<form:radiobutton path="gender" value="Female" cssClass="name" />
			<form:label path="gender" cssClass="name">FeMale</form:label>
			<form:errors cssClass="error" path="gender"></form:errors><br/>
			<form:label path="contactNumber" cssClass="name">Contact Number</form:label>
			<form:input path="contactNumber" class="form-control" />
			<form:errors cssClass="error" path="contactNumber" /><br>
			<form:label path="emailId" cssClass="name">Email Id</form:label>
			<form:input path="emailId" class="form-control" />
			<form:errors cssClass="error" path="emailId" /><br>
			<form:label path="password" cssClass="name">Password</form:label>
			<form:password path="password" class="form-control" />
			<form:errors cssClass="error" path="password" />
		</div><center><table><tr><td>
		<input type="submit" value="Submit" class="btn btn-primary"></td>
		<td><input type="reset" value="Reset" class="btn btn-primary"></td></tr></table></center>
</form:form>
</body>
</html> 

<%-- <div class="form-group">
<form:label path="firstName" cssClass="name">First Name</form:label><br/>
<form:input path="firstName" class="form-control"/><form:errors cssClass="error" path="firstName"/><br/>
</div>
<div class="form-group">
<form:label path="lastName">Last Name</form:label><br/>
<form:input path="lastName" class="form-control"/><form:errors cssClass="error" path="lastName"/><br/>
</div>
<div class="form-group">
<form:label path="age">Age</form:label><br/>
<form:input path="age" class="form-control"/><form:errors cssClass="error" path="age"/><br/>
</div>
<div class="form-group">
<form:label path="gender">Gender</form:label>
<form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female <form:errors cssClass="error" path="gender"></form:errors><br/>
</div>
<div class="form-group">
<form:label path="contactNumber">Contact Number</form:label><br/>
<form:input path="contactNumber" class="form-control"/><form:errors cssClass="error" path="contactNumber"/><br/>
</div>
<div class="form-group">
<form:label path="emailId">Email Id</form:label><br/>
<form:input type="email" path="emailId" class="form-control"/><form:errors cssClass="error" path="emailId"/><br/>
</div>
<div class="form-group">
<form:label path="password">Password</form:label><br/>
<form:password path="password" class="form-control"/><form:errors cssClass="error" path="password"/><br/>
</div>
<input type="submit" value="Submit" class="btn btn-primary">  --%>

 


