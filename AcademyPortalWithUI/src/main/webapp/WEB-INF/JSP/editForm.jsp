<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
 <h1>Edit Faculty</h1>  
<form:form action="/AcademyPortal/updateSave">    
 
<form:hidden path="associateId" /> 

<form:label path="firstName">First Name</form:label><br/>
<form:input path="firstName"/><br/>
<form:label path="lastName">Last Name</form:label><br/>
<form:input path="lastName" /><br/>
<form:label path="age">Age</form:label><br/>
<form:input path="age" /><br/>
<form:label path="gender">Gender</form:label>
<form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female <br/>
<form:label path="contactNumber">Contact Number</form:label><br/>
<form:input path="contactNumber" /><br/>
<form:label path="emailId">Email Id</form:label><br/>
<form:input type="email" path="emailId"/><br/> 
<input type="submit" value="Submit" class="btn btn-primary"> 
</form:form>    
</body>
</html>