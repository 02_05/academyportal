<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style>
.error{
color:red;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Faculty Login</title>
</head>
<body>
<div class="topnav">
  <a class="active" href="#">Academy Portal</a>
  <div class="topnav-right">
				<a href="register">Register Here</a>
			</div>
</div><br/><br/>
<img  class="img1" alt="image" src="resources/images/faculty1.jpg"><br/>
<!-- <p class="heading1">Login as a Faculty</p> -->
<form:form action="loginValidate" modelAttribute="registration" style="max-width: 350px; margin:0 auto;">
<div class="form-group">
<form:label path="firstName">First Name</form:label><br/>
<form:input path="firstName" class="form-control"/><form:errors cssClass="error" path="firstName"/><br/>
</div>
<div class="form-group">
<form:label path="password">Password</form:label><br/>
<form:password path="password" class="form-control"/><form:errors cssClass="error" path="password"/><br/>
</div>
<input type="submit" value="Submit" class="btn btn-primary"> 
</form:form>
</body>
</html>