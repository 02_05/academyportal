<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Batch Allocation</title>
<link rel="stylesheet" type="text/css" href="resources/CSS/style.css">
<style>
.error
{
color:red;
}
</style>
</head>
<body>
<div class="topnav">
  <a class="active" href="#">Academy Portal</a>
  <div class="topnav-right">
				<a href="index.jsp">Home</a>
			</div>
</div><br/><br/>
<form:form action="saveBatch" modelAttribute="batch" style="max-width: 450px; margin:0 auto;">
<%-- <div class="form-group">
<form:label path="batchId">Batch Id</form:label><br/>
<form:input path="batchId" class="form-control"/><form:errors cssClass="error" path="batchId"/><br/>
</div> --%>
<div class="form-group">
<form:label path="batchName">Batch Name</form:label><br/>
<form:input path="batchName" class="form-control"/><form:errors cssClass="error" path="batchName"/><br/>
</div>
<div class="form-group">
<form:label path="trainerName">Batch trainerName</form:label><br/>
<form:input path="trainerName" class="form-control"/><form:errors cssClass="error" path="trainerName"/><br/>
</div>
<div class="form-group">
<form:label path="noOfTrainees">Batch noOfTrainees</form:label><br/>
<form:input path="noOfTrainees" class="form-control"/><form:errors cssClass="error" path="noOfTrainees"/><br/>
</div>
<div class="form-group">
<form:label path="domain">domain</form:label><br/>
<form:input path="domain" class="form-control"/><form:errors cssClass="error" path="domain"/><br/>
</div>
<div class="form-group">
batchStartDate:<form:input type="date" path= "batchStartDate"/><form:errors cssClass="error" path="batchStartDate"/>
</div>
<div class="form-group">
batchEndDate:<form:input type="date" path= "batchEndDate"/><form:errors cssClass="error" path="batchEndDate"/>
</div>
<input type="submit" value="Submit"/>
</form:form>
</body>
</html>

