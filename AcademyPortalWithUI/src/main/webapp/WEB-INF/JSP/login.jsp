<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/CSS/style.css">
<style>
.error{
color:red;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Login</title>
</head>
<body>
<div class="topnav">
		<a class="active" href="#">Academy Portal</a>
		<div class="topnav-right">
			<a href="index.jsp">Home</a>
		</div>
	</div>
	<br />
	<br />

	<div>
		<img class="img1" alt="image"
			src="resources/images/secure-admin-1064328.png">
	</div>
	<br /> ${value}

<form:form action="userLogin" modelAttribute="login" style="max-width: 350px; margin:0 auto;">
<div class="form-group">
	<center>
		<p class="heading1">Login as a Admin User</p>
	</center>
<form:label path="userName" cssClass="name">UserName</form:label><br/>
<form:input path="userName" class="form-control"/><form:errors cssClass="error" path="userName"/><br/>
</div>
<div class="form-group">
<form:label path="password" cssClass="name">Password</form:label><br/>
<form:password path="password" class="form-control"/><form:errors cssClass="error" path="password"/><br/>
</div>
<input type="submit" value="Submit" class="btn btn-primary"> 
${value}
</form:form>
${value}
</body>
</html>

