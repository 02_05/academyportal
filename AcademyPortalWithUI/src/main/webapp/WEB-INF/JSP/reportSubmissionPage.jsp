<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="credit" modelAttribute="report">
<form:label path="batchId">Batch Id</form:label><br/>
<form:input path="batchId" value="${id}"/><br/>
<form:label path="rating">Rating</form:label><br/>
<form:input path="rating"/><br/>
<form:label path="noOfWorkingDays">No Of Working Days</form:label><br/>
<form:input path="noOfWorkingDays"/><br/>
<form:label path="statusOfReportSubmission">Status Of Report </form:label><br/>
<form:input path="statusOfReportSubmission"/><br/>
<%-- <form:checkbox path="statusOfReportSubmission"/>Yes
<form:checkbox path="statusOfReportSubmission"/>No<br/> --%>
<input type="submit" value="Submit">
</form:form>
</body>
</html>