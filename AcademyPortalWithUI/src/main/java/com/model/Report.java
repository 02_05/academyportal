package com.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//batchId,feedback,noOfWorkingDays,statusOfReportSubmission,FacultyCreditPoint
@Entity
@Table(name="Report_tbl")
public class Report {
	@Id
	private int id;
	private Integer batchId;
	private Integer rating;
	private Integer noOfWorkingDays;
	private String statusOfReportSubmission;
	private float facultyCreditPoint;
	public Report() {
		super();
		
	}
	public Report(Integer batchId, Integer rating, Integer noOfWorkingDays, String statusOfReportSubmission,
			float facultyCreditPoint) {
		super();
		this.batchId = batchId;
		this.rating = rating;
		this.noOfWorkingDays = noOfWorkingDays;
		this.statusOfReportSubmission = statusOfReportSubmission;
		this.facultyCreditPoint = facultyCreditPoint;
	}
	public Integer getBatchId() {
		return batchId;
	}
	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public Integer getNoOfWorkingDays() {
		return noOfWorkingDays;
	}
	public void setNoOfWorkingDays(Integer noOfWorkingDays) {
		this.noOfWorkingDays = noOfWorkingDays;
	}
	public String getStatusOfReportSubmission() {
		return statusOfReportSubmission;
	}
	public void setStatusOfReportSubmission(String statusOfReportSubmission) {
		this.statusOfReportSubmission = statusOfReportSubmission;
	}
	public float getFacultyCreditPoint() {
		return facultyCreditPoint;
	}
	public void setFacultyCreditPoint(float facultyCreditPoint) {
		this.facultyCreditPoint = facultyCreditPoint;
	}
	
}
