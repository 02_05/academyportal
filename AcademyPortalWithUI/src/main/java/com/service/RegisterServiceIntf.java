package com.service;


import java.util.List;

import com.model.Registration;



public interface RegisterServiceIntf {

	void saveDetails(Registration register);

	int checkUser(Registration registration);

	List<Registration> getFacultyDetails();

	void delete(int id);

	void edit(Registration register);

	Registration getDetails(int id);

	

}
