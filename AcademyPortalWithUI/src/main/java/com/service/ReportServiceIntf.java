package com.service;

import java.util.List;

import com.model.Batch;
import com.model.Report;

public interface ReportServiceIntf {

	List<Batch> getBatchId();

	void calculateCredit(Report report);

}
