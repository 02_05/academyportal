package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Batch;
import com.service.BatchServiceIntf;

@Controller
public class BatchController {
	@Autowired
	BatchServiceIntf service;

	@RequestMapping("/batch")
	public ModelAndView batchPage(@ModelAttribute("batch") Batch batch) {
		return new ModelAndView("batch");// jsp
	}

	@RequestMapping("/saveBatch")
	public ModelAndView saveBatch(@Validated @ModelAttribute("batch") Batch batch, BindingResult result) {
		if (result.hasErrors()) {
			return new ModelAndView("batch");
		} else {
			service.saveBatch(batch);
			return new ModelAndView("redirect:/displayUsers");
			// return new ModelAndView("success");
		}
	}
	
	@RequestMapping("/batchUpdate")
	public ModelAndView batchUpdatePage(@ModelAttribute("batch") Batch batch) {
		List<Batch> ls = service.getUsers();
		return new ModelAndView("batchUpdation", "listOfUsers", ls);
		
	}
	
	@RequestMapping(value="/updateBatch/{id}")    
    public ModelAndView edit(@PathVariable int id,Batch batch,Model model){
        batch=service.getDetails(id);
        model.addAttribute("command",batch);
        return new ModelAndView("batchEdit");    
    }    
    
    
    @RequestMapping("/batchSave")    
    public ModelAndView editSave(Batch batch){
        service.edit(batch);
        return new ModelAndView("redirect:/batchUpdate");    
    }   
    
    @RequestMapping(value="/deleteBatch/{id}")    
    public ModelAndView delete(@PathVariable int id){
        service.delete(id);
        return new ModelAndView("redirect:/batchUpdate");    
    }  

	@RequestMapping("/displayUsers")

	public ModelAndView getUsers() {
		List<Batch> ls = service.getUsers();
		return new ModelAndView("displayBatchDetails", "listOfUsers", ls);
	}

}
