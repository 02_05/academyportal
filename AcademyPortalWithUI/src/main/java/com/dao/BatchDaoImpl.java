package com.dao;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Batch;

 

@Repository

public class BatchDaoImpl implements BatchDaoIntf {
	//connection using hibernate
    @Autowired
    SessionFactory sessionFactory;
    //insert
    public void saveBatch(Batch batch) {
        sessionFactory.openSession().save(batch);
        
    }
    //fetch
    public List<Batch> getUsers()
    {
        List<Batch> ls=sessionFactory.openSession().createQuery("from Batch").list();
        return ls;
    }
	
	public Batch getDetails(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from batchdetails where batchId=:id").addEntity(Batch.class);
        query.setParameter("id",id);
        Batch batch=(Batch) query.uniqueResult();
        return batch;
	}

	public void edit(Batch batch) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update batchdetails set BatchName=:name,trainerName=:tname,noOfTrainees=:trainers,domain=:domain,batchStartDate=:sbatch,batchEndDate=:ebatch where batchId=:id").addEntity(Batch.class);
        query.setParameter("name",batch.getBatchName());
                query.setParameter("tname",batch.getTrainerName());
                query.setParameter("trainers",batch.getnoOfTrainees());
                query.setParameter("domain",batch.getDomain());
                query.setParameter("sbatch",batch.getBatchStartDate());
                query.setParameter("ebatch",batch.getBatchEndDate());
                query.setParameter("id",batch.getBatchId());
                query.executeUpdate();
	}
	
	public void delete(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("delete from batchdetails where batchId=:id").addEntity(Batch.class);
        query.setParameter("id",id);
        query.executeUpdate();
	}

}
