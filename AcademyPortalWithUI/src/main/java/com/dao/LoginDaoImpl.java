package com.dao;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Login;
import com.mysql.cj.Query;
@Repository
public class LoginDaoImpl implements LoginDao {
	@Autowired
	SessionFactory sessionFactory;
	public int usersCheck(Login login) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from login_tbl where userName=:name and password=:password").addEntity(Login.class);
		query.setParameter("name",login.getUserName());
		query.setParameter("password",login.getPassword());
		if(query.uniqueResult()==null) {
			return 0;
		}else {
			return 1;
		}
	}

	
	

}
