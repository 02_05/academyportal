<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
.error{
color:red;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
${value}
<form:form action="userLogin" modelAttribute="login">
<div class="form-group">
<form:label path="userName">UserName</form:label><br/>
<form:input path="userName" class="form-control"/><form:errors cssClass="error" path="userName"/><br/>
</div>
<div class="form-group">
<form:label path="password">Password</form:label><br/>
<form:password path="password" class="form-control"/><form:errors cssClass="error" path="password"/><br/>
</div>
<input type="submit" value="Submit" class="btn btn-primary"> 
${value}
</form:form>
${value}
</body>
</html>