package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Login;
import com.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService service;
	@RequestMapping("/login")
	public ModelAndView loginPage(@ModelAttribute("login")Login login) {
		return new ModelAndView("/login");
	}
	
	@RequestMapping("/userLogin")
	public ModelAndView loginValidate(@Validated @ModelAttribute("login")Login login,BindingResult result) {
		ModelAndView model=new ModelAndView();
		if(result.hasErrors()) {
			return new ModelAndView("/login");
		}else {
			int result1=service.checkUser(login);
			if(result1==1) {
				return new ModelAndView("/success");
			}else {
				model.addObject("value","Invalid credentials");
				model.setViewName("login");
				/*model.addObject("key","Invalid Credentials");*/
				return new ModelAndView("/login");
			}
		}
	}
	
}
