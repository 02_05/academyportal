package com.service;



import java.util.List;

 


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

 

import com.dao.BatchDaoIntf;
import com.model.Batch;
@Service
@Transactional
public class BatchServiceImpl implements BatchServiceIntf{
    @Autowired
    BatchDaoIntf dao;
    public void saveBatch(Batch batch) {
        dao.saveBatch(batch);
        
    }
    public List<Batch> getUsers()
    {
        List<Batch> ls=dao.getUsers();
        return ls;
    }
	
}
