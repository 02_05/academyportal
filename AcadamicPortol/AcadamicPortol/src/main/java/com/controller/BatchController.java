package com.controller;
import java.util.List;





import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

 

import com.model.Batch;
import com.service.BatchServiceIntf;

 

@Controller
public class BatchController{
     @Autowired
     BatchServiceIntf service;
    @RequestMapping("/batch")
    public ModelAndView batchPage(@ModelAttribute("batch") Batch batch)
    {
        return new ModelAndView("batch");//jsp
    }
    @RequestMapping("/saveBatch")
    public ModelAndView saveBatch(@Validated @ModelAttribute("batch") Batch batch, BindingResult result)
    {
        if(result.hasErrors())
        {
            return new ModelAndView("batch");
        }
        else
        {
            service.saveBatch(batch);
            return new ModelAndView("redirect:/fetchUsers");
            // return new ModelAndView("success");
        }
    }

 

    @RequestMapping("/fetchUsers")
    
    public ModelAndView getUsers()
    {
        List<Batch>ls=service.getUsers();
        return new ModelAndView("success","listOfUsers",ls);
    }
    }
