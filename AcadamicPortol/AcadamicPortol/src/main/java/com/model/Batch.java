package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name="batchdetails")
public class Batch {
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
private int batchId;
	 @NotEmpty(message="BatchName should not be empty")
     @Column(name="batchName",nullable=false)
private String batchName;
	 @NotEmpty(message="trainerName should not be empty")
private String trainerName;
	 @NotEmpty(message="NoOfTrainees should not be empty")
private int noOfTrainees;
	 @NotEmpty(message="domain should not be empty")
private String domain;
	 @NotNull(message="batchStartDate should not be empty")
     
     @DateTimeFormat(pattern = "yyyy-MM-dd")
     
private Date batchStartDate;
	 @NotNull(message="batchEndDate should not be empty")
     
     @DateTimeFormat(pattern = "yyyy-MM-dd")
   
private Date batchEndDate;
public int getBatchId() {
	return batchId;
}
public void setBatchId(int batchId) {
	this.batchId = batchId;
}
public String getBatchName() {
	return batchName;
}
public void setBatchName(String batchName) {
	this.batchName = batchName;
}
public String getTrainerName() {
	return trainerName;
}
public void setTrainerName(String trainerName) {
	this.trainerName = trainerName;
}
public int getnoOfTrainees() {
	return noOfTrainees;
}
public void setnoOfTrainees(int noOfTrainees) {
	noOfTrainees = noOfTrainees;
}
public String getDomain() {
	return domain;
}
public void setDomain(String domain) {
	this.domain = domain;
}
public Date getBatchStartDate() {
	return batchStartDate;
}
public void setBatchStartDate(Date batchStartDate) {
	this.batchStartDate = batchStartDate;
}
public Date getBatchEndDate() {
	return batchEndDate;
}
public void setBatchEndDate(Date batchEndDate) {
	this.batchEndDate = batchEndDate;
}

}
