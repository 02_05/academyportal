<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
.error{
color:red;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form:form action="loginValidate" modelAttribute="registration">
<div class="form-group">
<form:label path="firstName">First Name</form:label><br/>
<form:input path="firstName" class="form-control"/><form:errors cssClass="error" path="firstName"/><br/>
</div>
<div class="form-group">
<form:label path="password">Password</form:label><br/>
<form:password path="password" class="form-control"/><form:errors cssClass="error" path="password"/><br/>
</div>
<input type="submit" value="Submit" class="btn btn-primary"> 
</form:form>
</body>
</html>