<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.tablepro
{border-collapse:collapse;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body>
<table border="1" class="tablepro">
<tr><th>AssociateId</th><th>Faculty Name</th><th>Age</th><th>Gender</th><th>ContactNo</th><th>Email Id</th><th>Edit</th><th>Delete</th></tr>
<c:forEach var="user" items="${listOfUsers}">  
 <tr>
<td>${user.associateId}</td>
<td>${user.firstName} ${user.lastName}</td>
<td>${user.age}</td>
<td>${user.gender}</td>
<td>${user.contactNumber}</td>
<td>${user.emailId}</td>
<td><a href="update/${user.associateId}">Edit</a></td>
<td><a href="delete/${user.associateId}">Delete</a></td>
</tr> 
   </c:forEach>
</table> 
</body>
</body>
</html>