package com.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.ReportDaoIntf;
import com.model.Batch;
import com.model.Report;
@Service
@Transactional
public class ReportServiceImpl implements ReportServiceIntf{
	@Autowired
	ReportDaoIntf dao;
	public List<Batch> getBatchId() {
		List<Batch> list=dao.getBatchId();
		return list;
	}
	
	public void calculateCredit(Report report) {
		dao.calculateCredit(report);
		
	}

}
