package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Batch;
import com.model.Registration;
import com.model.Report;
import com.service.ReportServiceIntf;

@Controller
public class ReportController {
	@Autowired
	ReportServiceIntf service;
	@RequestMapping("/report")
	public ModelAndView reportPage(@ModelAttribute("batch") Batch batch,BindingResult result) {
		List<Batch> list=service.getBatchId();
		return new ModelAndView("reportScreen","batchId",list);
		
	}
	@RequestMapping("/reportSubmission")
	public ModelAndView reportSubmissionPage(@ModelAttribute("report") Report report,BindingResult result,Batch batch) {
		return new ModelAndView("reportSubmissionPage","id",batch.getBatchId());
	}
	@RequestMapping("/credit")
	public ModelAndView creditPoint(@ModelAttribute("report") Report report,BindingResult result) {
		service.calculateCredit(report);
		return new ModelAndView("success");
	}
}
