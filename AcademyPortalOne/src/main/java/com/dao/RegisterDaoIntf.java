package com.dao;


import java.util.List;

import com.model.Registration;

public interface RegisterDaoIntf {

	void saveDetails(Registration registration);

	int checkUsers(Registration registration);

	List<Registration> getFacultyDetails();

	void delete(int id);

	void edit(Registration register);

	Registration getDetails(int id);

	
}
