package com.dao;
import java.util.List;




import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

 

import com.model.Batch;

 

@Repository

public class BatchDaoImpl implements BatchDaoIntf {
	//connection using hibernate
    @Autowired
    SessionFactory sessionFactory;
    //insert
    public void saveBatch(Batch batch) {
        sessionFactory.openSession().save(batch);
        
    }
    //fetch
    public List<Batch> getUsers()
    {
        List<Batch> ls=sessionFactory.openSession().createQuery("from Batch").list();
        return ls;
    }

}
